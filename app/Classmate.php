<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classmate extends Model
{
    protected $guarded = [];

    function eventS()
    {

        return $this->belongsTo(EventS::class, 'event_ss_id', 'id');

    }


}
