<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventS extends Model
{

    protected $guarded = [];


    public static function incomplete()
    {
        return static::all();

    }

    function event_type_byid()

    {
        $event_type = $this->event_type()->where('id', $this->event_type_id)->get()->toArray();

        $a = $event_type[0]['name_' . getLocal()];

        return $a;


    }

    function duration_byid()
    {

        $event_type = $this->event_type()->where('id', $this->event_type_id)->get()->toArray();
        //dd($event_type[0]['name_'.'de']);

        return $event_type[0]['duration'];
    }

    function addClassMate($request)
    {

        $this->classmates()->create($request);

    }

    function place_byid()

    {
        $place = $this->place()->where('id', $this->place_id)->get()->toArray();
        //dd($event_type[0]['name_'.'de']);
        $a = $place[0]['name_' . getLocal()];

        return $a;


    }

    function event_type()
    {

        return $this->belongsTo(EventType::class);

    }

    function place()
    {

        return $this->belongsTo(Place::class);

    }

    function classmates()
    {

        return $this->hasMany(Classmate::class, 'event_ss_id');
    }

}

