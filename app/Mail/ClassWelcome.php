<?php

namespace App\Mail;

use App\Classmate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ClassWelcome extends Mailable
{
    use Queueable, SerializesModels;
    public $classmate;
    public $place;
    public $event_type;
    public $date;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Classmate $classmate)
    {

        $this->classmate=$classmate;
        $this->date=$classmate->eventS->date;
        $this->event_type=$classmate->eventS->event_type_byid();
        $this->place=$classmate->eventS->place_byid();

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.classwelcome'.getLocal());
    }
}
