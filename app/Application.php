<?php
/**
 * Created by PhpStorm.
 * User: andpas
 * Date: 24.05.18
 * Time: 21:27
 */

namespace App;


class Application extends \Illuminate\Foundation\Application
{
    public function publicPath()
    {
        return $this->basePath.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'public_html';
    }



}