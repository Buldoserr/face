<?php

namespace App\Http\Middleware;

use Closure;
use League\Flysystem\Config;
use App;
use Session;


class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//
//        $raw_local= Session::get('locale');



        if (session()->has('locale'))
        $raw_local=session('locale');
        else
            $raw_local=App::getLocale();
//
//
//        if (in_array($raw_local, Config::get('app/locales'))){
            $locale = $raw_local;

//        } else
//                {
//                    $locale=Config::get('locale');
//                }
//
//var_dump("jopa");

        App::setLocale($locale);
        //dd(Session::all());
        //dd(session('1'));
        //dd($raw_local,$locale,Session::all(),App::getLocale());
        //dd(App::getLocale());
        $response=$next($request);
        return $response;
    }
}
