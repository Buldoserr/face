<?php

namespace App\Http\Controllers;

use App\EventS;
use App\EventType;
use App\Place;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EventSController extends Controller
{

    public function __construct()
    {

        $this->middleware('auth', ['except' => ['show']]);

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
//        $date=date('Y-m-d H:i:s',time());
//        $events= EventS::all()->where('date','>',$date);

        $events = EventS::all()->sortByDesc('date');
        $places = Place::all();
        $event_types = EventType::all();

        return view('classes.index', compact('events', 'places', 'event_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        //валидация на стороне сервера
        //по хорошему надо разработать view error, чтобы показывать общие ошибки
        $this->validate(request(),
            [

                'date' => "required",
                'place_id' => "required",
                'event_type_id' => "required",
            ]);


        EventS::create([

            'date' => request('date'),

            'place_id' => request('place_id'),

            'event_type_id' => request('event_type_id'),

        ]);

        return redirect('/classes');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\event $event
     * @return \Illuminate\Http\Response
     */
    public function show(EventS $event)
    {
        // выдача для списка мероприятий, выдаём те мероприятия которые поосле сегодняшнего дня в порядке возрастания

        $date = date('Y-m-d H:i:s', time());
        $events = EventS::all()
            ->where('date', '>', $date)
            ->sortBy('date');
        //здесь меняем дату на формат в d-M\ H:i
        foreach ($events as $eventdate) {
            $eventdate->date = (new \DateTime($eventdate->date))->format('d-M\ H:i');
        }

        $places = Place::all();
        $event_types = EventType::all();
//dd($events);
        return view('contentpages.schedule', compact('events', 'places', 'event_types'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\event $event
     * @return \Illuminate\Http\Response
     */
    public function edit(EventS $event)
    {

        $places = Place::all();
        $event_types = EventType::all();

//     записываем дату в формате, которую принимает элемент datetime-local
        $event->date = (new \DateTime($event->date))->format('Y-m-d\TH:i:s');

        return view('classes.editcl', compact('event', 'places', 'event_types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\event $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EventS $event)
    {
        //для редактирования событий
        $this->validate(request(),
            [

                'date' => "required",
                'place_id' => "required",
                'event_type_id' => "required",


            ]);


        $event->date = request('date');
        $event->place_id = request('place_id');
        $event->event_type_id = request('event_type_id');

        $event->save();


        return redirect('/classes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\event $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(EventS $event)
    {
        //
    }
}
