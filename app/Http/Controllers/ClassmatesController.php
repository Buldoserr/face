<?php

namespace App\Http\Controllers;

use App\Classmate;
use App\EventS;
use App\Mail\ClassWelcome;
use Illuminate\Http\Request;

class ClassmatesController extends Controller
{

    public function __construct()
    {

        $this->middleware('auth', ['except' => ['show', 'store']]);

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(EventS $event)
    {
        //возвращает список зарегистриорванных на мероприятие
        // надо добавить вьюшку
        $classmates=$event->classmates;


        return view('cabinet.classmates',compact('classmates','event'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, EventS $event)
    {
        //вызов post происходит так
        //Route::post('/register/{event}', 'ClassmatesController@store');
        //так что надо у Жени уточнить а можно ли подсунуть ивент_id не правильный
        //проверяем заполнение формы в лайтовом режиме пока не покурил мануалы по validate
        //
        $this->validate(request(),
            [
                'name' => "required|min:2",
                'email' => "required|email",
                'phone' => "required"

            ]);

        if ($event->date<date( time()))
        return back()->withErrors([
           'message' => 'Please check your event date and try again!']
        );
        //сохраняем участника в базе


        ///поправить  на ассоциацию. ассоциация работает. но не получается массовое присваивание т.к.
        ///  методу create нужны все поля, а у нас в реквесте нет поля с event_ss_id
        ///  тогда сделал добавление через метод addClassMate класса EventS
        ///  осталось понять а как этому новому отправить письмо.
//        $classmate->event_ss_id = $event->id;
//        $classmate = new Classmate();
//        $classmate->eventS()->associate($event);
//        $classmate->name = $request['name'];
//        $classmate->email = $request['email'];
//        $classmate->phone = $request['phone'];
//
//        $classmate->save();
        //добавляем участника через метода addClassMate класса EventS
//        $event->addClassMate(request(['name', 'email', 'phone']));
        //можно не выносить в отдельный метод, а  здесь и создать

        $classmate = $event->classmates()->create(request()->all());
        //шлём приветственный емайл
        \Mail::to($classmate->email)->send(new ClassWelcome($classmate));
        // надо отправить его на страницу с текстом письма и сказать что к вам ушло письмо.
        return redirect('/');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Classmate $classmates
     * @return \Illuminate\Http\Response
     */
    public function show(EventS $event, Classmate $classmates)
    {


        return view('contentpages.registerclass', compact('event'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Classmate $classmates
     * @return \Illuminate\Http\Response
     */
    public function edit(Classmate $classmates)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Classmate $classmates
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Classmate $classmates)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Classmate $classmates
     * @return \Illuminate\Http\Response
     */
    public function destroy(Classmate $classmates)
    {
        //
    }
}
