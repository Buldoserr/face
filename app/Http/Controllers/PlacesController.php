<?php

namespace App\Http\Controllers;

use App\Place;
use Illuminate\Http\Request;

class PlacesController extends Controller
{

    public function __construct()
    {

        $this->middleware('auth', ['except' => ['show']]);

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $places = Place::all();
        return view('place.index', compact('places'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//
        //        dd($request);
        $this->validate(request(),
            [
                'name_ru' => "required",
                'name_de' => "required",
                'name_en' => "required",


            ]);

        Place::create([
            'name_ru' => request('name_ru'),

            'name_de' => request('name_de'),

            'name_en' => request('name_en'),

        ]);
//
        return redirect('/places');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Place $places
     * @return \Illuminate\Http\Response
     */
    public function show(Place $places)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Place $places
     * @return \Illuminate\Http\Response
     */
    public function edit(Place $places)
    {
        return view('place.editet', compact('places'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Place $places
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Place $places)
    {

        $this->validate(request(),
            [
                'name_ru' => "required",
                'name_de' => "required",
                'name_en' => "required",

            ]);

        //take eventType and equeel fields

        $places->name_ru = request('name_ru');
        $places->name_de = request('name_de');
        $places->name_en = request('name_en');

        //than save
        $places->save();
        return redirect('/places');//
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Place $places
     * @return \Illuminate\Http\Response
     */
    public function destroy(Place $places)
    {
        //
    }
}
