<?php

namespace App\Http\Controllers;

use App\EventType;
use App\EventS;
use Illuminate\Http\Request;

class EventTypeController extends Controller
{

    public function __construct()
    {

        $this->middleware('auth', ['except' => ['show']]);

    }


    public function index()
    {

        $event_type = EventType::all();

        return view('events.index', compact('event_type'));//
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(),
            [
                'name_ru' => "required",
                'duration' => "required"

            ]);

        EventType::create([
            'name_ru' => request('name_ru'),
            'name_de' => request('name_de'),
            'name_en' => request('name_en'),
            'duration' => request('duration'),
        ]);
//
        return redirect('/events');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EventType $eventType
     * @return \Illuminate\Http\Response
     */
    public function show(EventType $eventType)
    {

        $events = EventS::all()->where('event_type_id', '=', $eventType->id);
//        $events = EventS::find($eventType->id);
//        dd($events);
        return null;

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EventType $eventType
     * @return \Illuminate\Http\Response
     */
    public function edit(EventType $eventType)
    {
        return view('events.editet', compact('eventType'));//
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\EventType $eventType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EventType $eventType)
    {
        $this->validate(request(),
            [
                'name_ru' => "required",
                'duration' => "required"

            ]);

        //take eventType and equeel fields

        $eventType->duration = request('duration');
        $eventType->name_ru = request('name_ru');
        $eventType->name_de = request('name_de');
        $eventType->name_en = request('name_en');


        //than save
        $eventType->save();
        return redirect('/events');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EventType $eventType
     * @return \Illuminate\Http\Response
     */
    public function destroy(EventType $eventType)
    {
        //
    }
}
