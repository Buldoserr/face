<?php

use Faker\Generator as Faker;

$factory->define(App\Place::class, function (Faker $faker) {
    return [
        'name_ru'=>$faker->country,
        'name_de'=>$faker->country,
        'name_en'=>$faker->country
            ];
});

