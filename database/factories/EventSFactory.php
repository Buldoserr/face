<?php

use Faker\Generator as Faker;

$factory->define(App\EventS::class, function (Faker $faker) {
    return [
        'place_id'=>function(){
        return factory(App\Place::class)->create()->id;
        },
        'event_type_id'=>function(){
        return factory(App\EventType::class)->create()->id;
        },
        'date'=>$faker->dateTime
    ];
});
