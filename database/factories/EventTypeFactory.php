<?php

use Faker\Generator as Faker;

$factory->define(App\EventType::class, function (Faker $faker) {
    return [
        'name_ru'=>$faker->sentence,
        'name_de'=>$faker->sentence,
        'name_en'=>$faker->sentence,
        'duration'=>$faker->numberBetween([1],[4])

    ];
});
