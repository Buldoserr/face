<?php

use Faker\Generator as Faker;

$factory->define(App\Classmate::class, function (Faker $faker) {
    return [
        'event_ss_id'=> function () {
            return factory(App\EventS::class)->create()->id;
        },
        'name'=>$faker->name,
        'email'=>$faker->email,
        'phone'=>$faker->phoneNumber
    ];
});