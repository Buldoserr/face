<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return view('contentpages.ffhome');

});

Route::get('/about', function () {

    return view('contentpages.ffabout');
});

Route::get('/beforeafter', function () {
    return view('contentpages.ffbeforeafter');
});
Route::get('/schedule', 'EventSController@show');

Route::get('/enterbonus', function () {
    return view('contentpages.bonus');
});

Route::get('/privacy', function () {
    return view('contentpages.privacy');
});

Route::get('/imprint', function () {
    return view('contentpages.legalnotice');
});

Route::get('/lang/{locale}', function ($locale) {

    session(['locale' => $locale]);

    return redirect()->back();
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::post('/events', 'EventTypeController@store');

Route::get('/events', 'EventTypeController@index');

Route::post('/events/{event_type}', 'EventTypeController@update');

Route::get('/events/{event_type}', 'EventTypeController@edit');


Route::post('/places', 'PlacesController@store');

Route::get('/places', 'PlacesController@index');

Route::post('/places/{places}', 'PlacesController@update');

Route::get('/places/{places}', 'PlacesController@edit');


Route::post('/classes', 'EventSController@store');

Route::get('/classes', 'EventSController@index');

Route::post('/classes/{event}', 'EventSController@update');

Route::get('/classes/{event}', 'EventSController@edit');


Route::get('/register/{event}', 'ClassmatesController@show');

Route::post('/register/{event}', 'ClassmatesController@store');

Route::get('/register/list/{event}', 'ClassmatesController@index');

Route::get('/cabinet', function () {

    return view('cabinet.home');

})->middleware('auth');

//Route::get('/register/list/{eventType}','EventTypeController@show');

//Route::get('/emailtest', function () {
//    Mail::raw('Sending amails with mailgun', function ($message) {
//
//        $message->to('and.pas@hotmail.com');
//        $message->subject('Mailgun testing!');
//        $message->from('no-reply@face-fitness.com');
//
//    });
//});