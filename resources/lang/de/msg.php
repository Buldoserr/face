<?php


return [
    'aboutface_header'=> "Was ist Face-Fitness",
    'aboutface_header1'=> "
     <p>
            <b>Face-Fitness </b> ist eine sehr effektive Gymnastik für das Gesicht. Es besteht aus einer einzigartigen
            Reihe von Übungen, die darauf abzielen, die Gesichtsmuskeln auf eine spezifische Art und Weise zu
            trainieren. Damit wird nicht nur die Muskulatur im Gesicht aufgebaut, sondern es wird sich darum gekümmert,
            diese sorgfältig und mit Hilfe von ausgewählten Übungen, im Gleichgewicht zu halten. Schwache Muskeln werden
            gestärkt und angespannte entspannt. </p>
        <p>
            Körper-Fitness hat einen sehr hohen Stellenwert in unserer Gesellschaft, allerdings wird das Gesicht dabei
            vergessen. Und dabei ist das Gesicht unsere Visitenkarte! Face-Fitness, die spezielle Gymnastik für die
            Gesichtsmuskulatur, hilft Falten zu glätten, macht die Haut elastischer, und die Gesichtszüge werden
            gestrafft und der Tonus des muskulösen Gesichtsrahmens zurückgegeben.<br>
            Mit Hilfe von Face-Fitness erreichen Sie:</p>


        <ul tyle='list-style-type: none;'>
            <li><span>✓</span> eine gesunde Haut, die nicht mit einer Abdeckcreme bedeckt werden muss</li>
            <li><span>✓</span> eine Verbesserung der Hautfarbe</li>
            <li><span>✓</span> eine verbesserte Kontur des Gesichts</li>
            <br>
            <li><span>✓</span> ein perfektes Aussehen vor einem wichtigen Ereignis</li>
            <li><span>✓</span> eine Verbesserung der Nasolabialfalten, Zornesfalten, des Doppelkinns (Lefzen und
                Schwellungen sind damit Geschichte)
            </li>
            <li><span>✓</span> die Falten auf der Stirn und um die Augen werden beseitigt, die Augenlider werden
                gestrafft
            </li>

            <li><span>✓</span> die Schwellungen und Tränensäcke unter den Augen werden entfernt</li>
            <li><span>✓</span> der Prozess der Augenabsackung wird verlangsamt</li>
            <li><span>✓</span> die Kontur und das Volumen der Lippen wird verändert</li>
            <li><span>✓</span> die Wangenknochen werden ausdrucksvoller</li>
            <li><span>✓</span> die Falten am Hals geglättet</li>
            <br>
            <li><span>✓</span> es werden Energietechniken verwendet, um die Stimmung zu verbessern und die Selbstliebe
                zurückzugewinnen
            </li>
            <li><span>✓</span> Immer perfekt auf Selfies aussehen, ohne Filter zu verwenden</li>
        </ul>
        <p>
            Die Muskeln im Gesicht benötigen Nahrung, Sauerstoff und Bewegung. Sie sind sehr sanft und geschmeidig. Es
            ist wichtig Muskeln pflegend und sorgfältig zu behandeln, denn auch die Creme wird auf spezielle Linien
            aufgetragen. Das Ziel der Face-Fitness ist die korrekte Gewebespannung auszubalancieren.<br>

            Face-Fitness kann in jedem Alter betrieben werden, es ist eine Philosophie, die einen das ganze Leben
            begleitet! Es ist die Kultur der Schönheit, Gesundheit und Jugend.
        </p>
    ",
    'aboutme_header1'=>"
    <p>
            Ich heiße Anastasia Kirchenmajer und bin 34 Jahre alt. <br>
            Ich bin zertifizierter Trainer für Anti-Aging-Praktiken für das Gesicht nach der Methode von Alena
            Rossoschinskaya - I love Face-Fitness.
            Ich wollte mich schon immer Mode, Schönheit und der Kommunikation mit Frauen widmen.
            Ich liebe es mit Menschen zu kommunizieren, in der Vergangenheit habe ich in der Tourismus-Branche
            gearbeitet. Ich hatte die vielfältige Möglichkeit verschiedene Länder zu bereisen und Mentalitäten sowie die
            Feinheiten der verschiedenen Kulturen kennenzulernen.
            Eines Tages, ich war gerade 30 Jahre alt und hatte mein erstes Kind zur Welt gebracht, habe ich mich vor
            meinem Spiegelbild erschrocken über die ersten Anzeichen eines \"müden\" Gesichtsausdrucks und einiger
            ungebetener Gäste in meinem Gesicht.
            Zudem machte mich das Auftreten erster Falten unzufrieden;
            die ersten Anzeichen des Alterungsprozesses im Gesicht beginnen bereits mit 25 Jahren, im Alter von 30
            Jahren beginnt unser Gesicht langsam nach unten zu rutschen.
        </p>
        <p>
            Plastische Kosmetik oder Injektionen kamen für mich nicht infrage und zum selben Zeitpunkt fiel mir
            Face-Fitness buchstäblich vor die Füße, oder besser gesagt ins Gesicht. Nachdem ich angefangen hatte
            Face-Fitness zu betreiben und dann auch erste Kurse besuchte, kehrte einerseits schnell die Jugend in mein
            Gesicht zurück und ich fing an, mein Spiegelbild wieder zu mögen, selbst ab dem frühen Morgen glänzte mein
            Gesicht und die falten wurden deutlich weniger. Andererseits hatte ich den Spaß an Face-Fitness entdeckt und
            mich entschlossen, Face-Fitness ernsthaft und professionell anzugehen. Heute bin ich zertifizierter Coach
            für Face-Fitness und verjüngende Gesichtspraktiken
        </p>
        <p>
            Das erste Training sollte unter Anleitung eines erfahrenen Trainers durchgeführt werden, der
        </p>
        <ul style='list-style-type: none;'>
            <li> <span>✓</span>  auf Fehler hinweist, die leicht übersehen werden können, wenn man Gesichtsgymnastik nur aus dem
                Internet oder Büchern betreibt
            </li>
            <li> <span>✓</span> ein speziell auf die individuellen Gesichtszüge ein persönlich abgestimmtes Programm entwickelt</li>
            <li><span>✓</span> zum Weitermachen inspiriert und motiviert</li>
        </ul>
        <p> Ich habe diesen Beruf für mich gewählt, um Schönheit in die Welt zu tragen, Frauen dabei zu helfen, auf sich
            selbst zu achten und jugendliches Aussehen und Gesundheit zu erhalten oder zurückzubringen. Und das alles
            auf natürliche Art Weise mit Hilfe der Hände und ohne chirurgische Eingriffe oder Injektionen. Das Gesicht
            erhält einen lebendigen Ausdruck, der, egal ob lachend, traurig oder wütend, seine Jugend und Natürlichkeit
            behält. Ein gesunder Gesichtsausdruck ohne Falten sorgt für mehr Selbstvertrauen, Kommunikation und auch
            positive Botschaften.<br>
            Übungen für das Gesicht helfen gut auszusehen und sich gut zu fühlen, das Beste aus sich selbst zu machen.
        </p>
    ",
    'before_header' => "Welche Ergebnisse erzielen die Teilnehmer meiner Workshops?",
    'before_header1' => "
     <p>
            Face-Fitness ist bereits ein wichtiges Instrument der Beauty Industrie, Hunderttausende von Frauen auf der
            ganzen Welt betreiben es bereits und folgen sogenannten \"Self-Care-Feen\". Bevor Sie mit Face-Fitness
            beginnen und sich dazu entschließen an Workshops teilzunehmen, betrachten Sie die Ergebnisse, die durch
            Personal Training entstanden sind. </p>
        <p>
            Um die Frage zu beantworten, ob Face-Fitness auch für Sie infrage kommt, haben wir Ihnen einige kurze Fragen
            zusammengestellt:
        </p>
        <ul style='list-style-type: none'>
            <li><span>✓</span>Wollen Sie noch besser aussehen und sich besser fühlen?</li>
            <li><span>✓</span>Sind Sie gegen Injektionen und Operationen?</li>
            <li><span>✓</span> Möchten Sie Geld für kosmetische Eingriffen sparen und keine Zeit mit Aufzeichnungen und
                deren Erwartungen verschwenden?
            </li>
            <li><span>✓</span> Möchten Sie Qualität Ihre Haut verbessern, Falten glätten und das Gesichtsoval straffen.
            </li>

        </ul>
    ",
    'before_header2'=>"Was ist Schönheit? <span>Und welchen Preis hat sie?</span>",
    'before_header3'=>"
    <p>
            Es ist allgemein bekannt, dass Schönheit ein teureres Vergnügen ist. Und natürliche Schönheit wird nicht
            jedem gegeben. Auf der Suche nach Schönheit ist das weibliche Geschlecht bereit, den letzten Cent zu opfern.
            Nicht viele Frauen glauben, dass Face-Fitness nur wenig Zeit und Fleiß erfordern, dafür aber einen guten
            Coach. Er wird Ihnen helfen, die für Sie richtigen Übungen zu finden, ihre Hände richtig zu platzieren und
            erklären, welche Gefühle Sie bei den Übungen haben sollten.<br>
            Wenn Sie einmal die richtige Funktionsweise Ihrer Muskeln erlernt haben und wissen, damit umzugehen, werden
            Sie nie mehr daran denken, einen Kosmetiker für Injektionen oder gar einen plastischen Chirurgen
            aufzusuchen. <br>
            Es gibt aber auch einige Dinge, die nicht mit Face-Fitness korrigiert werden können.
            ABER! Das Beibehalten einer schönen Gesichtskontur, das Öffnen der Augen, das Verhindern von Tränensäcken
            sowie der Schwerkraft, das Glätten der Nasolabial- und Zornesfalten, können durch das Training mit einem
            professionellen Coach erreicht werden. Danach ist es unter Anleitung einfach die gewünschten Ziele zu
            erreichen. Die Zeit und das Geld sind im Vergleich zu anderen Vorgehensweise keine große Investition. Ich
            wünsche Ihnen ein schönes Gesicht!
        </p>
    ",
    'classes_header1'=>"Face-Fitness Basic",

    'classes_header2'=>"Face-Fitness für Fortgeschrittene",
    'classes_header3'=>"Vakuum Masterclass",
    'classes_header4'=>"Lifting Komplex",
    'classes_content1'=>"
     <p> Der Basis Kurs mit Übungen für die problematischsten Bereiche des Gesichts mit einem vollständigen Satz an
            Face-Fitness Übungen für die Form des Gesichts, Hals, Wangen, Augenbereich, Stirn, Kinn und Lippen. <br>
            Ideal für alle, die: </p>
       <br>

        <ul style='list-style-type: none '>
            <li><span>✓</span> mit Face-Fitness beginnen möchten</li>
            <li><span>✓</span> andere Methoden erfolglos versucht haben</li>
            <li><span>✓</span> Injektionen und andere Eingriffe versucht haben, aber nicht das gewünschte Ergebnis
                erzielt haben.
            </li>

        </ul>
         <br>
        <p>Preis pro Kurs:</p>
        <ul style='list-style-type: none '>
            <li><span>✓</span> 130 Euro / Person - ab einer Gruppe von 5 Personen</li>
            <li><span>✓</span> 200 Euro - Personal Training</li>
        </ul>
        </p>
    ",
    'classes_content2'=>"
        <p> Übungen mit Wow-Effekt zur Stärkung des Basis Kurses.
            Fortgeschrittene, intensive Übungen zur Gesichtsmodellierung: glättet Falten auf der Stirn und im Bereich der Nasolabialfalten, strafft die Gesichtskontur, reduziert das Doppelkinn, ermöglicht eine spürbare Lidstraffung. <br>
            Geeignet für alle, die:</p>

        
        <ul style='list-style-type: none '>

            <li><span>✓</span> den Basis Kurs absolviert haben</li>
            <li><span>✓</span> die das nächste Level im Face-Fitness erreichen möchten</li>
            <li><span>✓</span> die Techniken erweitern und verfeinern möchten, die Durchführung der Übungen kontrollieren und die nächste Stufe mit neuen Übungen erreichen möchten.</li>

        </ul>
        
        <p>
            Preise pro Kurs:</p>
        <ul style='list-style-type: none '>
            <li><span>✓</span>120 Euro / Person - ab einer Gruppe von 4 Personen</li>
            <li><span>✓</span>200 Euro - Personal Training
            </li>
        </ul>
    ",
    'classes_content3'=>"
        <p>Sofortige Wirkung von Super-Lifting und Recovery. Sie gibt der Haut sofort einen guten Teint, Farbe,
            vertreibt Schwellungen, \"setzt\" die Muskeln an Ort und Stelle, kämpft effektiv gegen altersbedingte
            Manifestationen.
            Es wird empfohlen, die Massage nicht öfter als 1 bis 3 Mal pro Woche durchzuführen. Verfügbar für alle mit
            einem absolvierten Basis Kurs, aber unabhängig von der Erfahrung. <br>
            Für alle, die:</p>
        <ul style='list-style-type: none '>
            <li><span>✓</span> sofort den größtmöglichen Effekt erzielen möchten.</li>

            <li><span>✓</span> ie lernen möchten, wie sie ihrer Haut und Vitalität schnell ein frisches und gesundes
                Aussehen nach einer schlaflosen Nacht oder Stress zurückbringen können.
            </li>

        </ul>
        <div class='alert alert-info'>Die Vakuum-Geräte sind in den Kosten der Vakuum Masterclass enthalten und
            verbleiben danach bei Ihnen.
        </div>

        <p>Preise pro Kurs:</p>
        <ul style='list-style-type: none '>

            <li><span>✓</span> 120 Euro / Person - ab einer Gruppe von 4 Personen</li>
            <li><span>✓</span> 200 Euro - Personal Training</li>
        </ul>
        </p>
    ",
    'classes_content4'=>"
    <p>
            Ein ganz neues Lebensgefühl. Lernen Sie Ihre Gesichtsmimik kennen und kontrollieren und wie Sie z. B.
            Glücksgefühle, Traurig sein oder lacht ohne Falten zu vertiefen.
            Der effektiver Lifting Komplex hilft Ihnen neue mimische Gewohnheiten auszubilden, das ist die beste
            Vorbeugung vor altersbedingten Veränderungen und Stress.
            Im Anschluss an den Basis Kurs, unabhängig von der Erfahrung.<br>
            Für alle, die:
        </p>
        <ul style='list-style-type: none '>
            <li><span>✓</span> Übungen mit einem schnellen sichtbaren Straffungseffekt erzielen möchten</li>


            <li><span>✓</span> Lernen wollen, den Gesichtsausdruck in jeder Lebenslage und in jeder Stimmung zu
                kontrollieren, z. B. bei Terminen, Geschäftsverhandlungen oder in Konfliktsituationen
            </li>
        </ul>

        <br>
        <p>Preis pro Kurs:</p>
        <ul style='list-style-type: none '>
            <li><span>✓</span> 90 Euro / Person - ab einer Gruppe von 4 Personen</li>
            <li><span>✓</span> 170 Euro - Personal Training</li>
        </ul>

    ",
    'carousel_header1'=>"Face Fitness ab jetzt auch in München!",
    'carousel_content1'=>"An Ihrem Alter können wir nichts ändern, aber wir helfen ihnen,  jünger 
auszusehen!",
    'carousel_header2'=>"Wollen auch Sie schön alt werden?",
    'carousel_content2'=>"Machen Sie sich selbst ein Geschenk und schenken sich Jugend.",
    'carousel_header3'=>"Machen Sie sich selbst ein Geschenk und schenken sich Jugend.",
    'carousel_content3'=>"Ohne Kosmetik oder plastische Chirurgie, nur mit Hilfe Ihrer Hände und Gesichtsmuskulatur.",
        'schedule_header1'=>"Meine nexten Workshops",
        'schedule_header2'=>"Bitte kontaktiren Sie mich für personliche Termin absprachen:",
        'register_header1'=>"Anmeldung für das Workshop.",
];
