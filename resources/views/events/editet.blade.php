@extends('layouts.mastercab')
@section('content')
<div class="col-md-8 blog-main">
    <H1>Редактирование типов мастер-классов.</H1>

    <hr>

    <form method="POST" action="/events/{{$eventType->id}}">
        {{csrf_field()}}


        <div class="form-group">

            <label for="name_ru">Название мастер-класса на русском</label>
            <input name="name_ru" id="name_ru" cols="30" rows="10" class="form-control" value="{{$eventType->name_ru}}" required >

        </div>

<br>
        <div class="form-group">

            <label for="name_de">Название мастер-класса на немецком</label>
            <input name="name_de" id="name_de" cols="30" rows="10" class="form-control" value="{{$eventType->name_de}}" required >

        </div>
        <br>

        <div class="form-group">

            <label for="name_en">Название мастер-класса на английском</label>
            <input name="name_en" id="name_en" cols="30" rows="10" class="form-control" value="{{$eventType->name_en}}" required >

        </div>
        <br>

        <div class="form-group">

            <label for="duration">Длительность мастер-класса в часах</label>
            <input name="duration" id="duration" cols="30" rows="10" class="form-control" value="{{$eventType->duration}}" required >

        </div>



        <div class="form-group">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
        {{--@include('layouts.errors')--}}
    </form>
</div>
    @endsection