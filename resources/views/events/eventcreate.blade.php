<div class="col-md-8 blog-main">
    <H1>Создание новых мастер-классов!</H1>

    <hr>

    <form method="POST" action="/events">
        {{csrf_field()}}


        <div class="form-group">

            <label for="name_ru">Название мастер-класса на русском</label>
            <input name="name_ru" id="name_ru" cols="30" rows="10" class="form-control" required >

        </div>

<br>
        <div class="form-group">

            <label for="name_de">Название мастер-класса на немецком</label>
            <input name="name_de" id="name_de" cols="30" rows="10" class="form-control" required >

        </div>
        <br>

        <div class="form-group">

            <label for="name_en">Название мастер-класса на английском</label>
            <input name="name_en" id="name_en" cols="30" rows="10" class="form-control" required >

        </div>

        <br>

        <div class="form-group">

            <label for="duration">Длительность мастер-класса в часах</label>
            <input type="text" name="duration" id="duration" cols="30" rows="10" class="form-control" required ></input>


        </div>

        <br>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
        {{--@include('layouts.errors')--}}
    </form>
</div>