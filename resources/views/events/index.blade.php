@extends('layouts.mastercab')
@section('content')
@include('events.eventcreate')

<table class="table table-sm">
    <thead>
    <tr>
        <th scope="col">Duration</th>
        <th scope="col">Class name RU</th>
        <th scope="col">Class name DE</th>
        <th scope="col">Class name EN</th>
        <th scope="col">Edit</th>
    </tr>
    </thead>
    <tbody>
    @foreach( $event_type as $eventf)

        @include('events.event')

    @endforeach

    </tbody>
</table>

@endsection

