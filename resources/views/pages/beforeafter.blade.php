<div id="results" class="row ">
    <div class="col-md-6 rounded-heading lead">
        <h2 class="featurette-heading">
            @lang("msg.before_header")
        </h2>
        @lang("msg.before_header1")
        {{--<p>--}}
            {{--Face-Fitness ist bereits ein wichtiges Instrument der Beauty Industrie, Hunderttausende von Frauen auf der--}}
            {{--ganzen Welt betreiben es bereits und folgen sogenannten "Self-Care-Feen". Bevor Sie mit Face-Fitness--}}
            {{--beginnen und sich dazu entschließen an Workshops teilzunehmen, betrachten Sie die Ergebnisse, die durch--}}
            {{--Personal Training entstanden sind. </p>--}}
        {{--<p>--}}
            {{--Um die Frage zu beantworten, ob Face-Fitness auch für Sie infrage kommt, haben wir Ihnen einige kurze Fragen--}}
            {{--zusammengestellt:--}}
        {{--</p>--}}
        {{--<ul style='list-style-type: none'>--}}
            {{--<li><span>✓</span>Wollen Sie noch besser aussehen und sich besser fühlen?</li>--}}
            {{--<li><span>✓</span>Sind Sie gegen Injektionen und Operationen?</li>--}}
            {{--<li><span>✓</span> Möchten Sie Geld für kosmetische Eingriffen sparen und keine Zeit mit Aufzeichnungen und--}}
                {{--deren Erwartungen verschwenden?--}}
            {{--</li>--}}
            {{--<li><span>✓</span> Möchten Sie Qualität Ihre Haut verbessern, Falten glätten und das Gesichtsoval straffen.--}}
            {{--</li>--}}

        {{--</ul>--}}
        {{--<p>Могу поспорить, что Вы ответили Да минимум на 2-3 вопроса и в этом случае, Фейс фитнес Вам нужен. Фейс фитнес--}}
            {{--действительно работает! Более того результат от фейс фитнеса виден сразу после тренинга ! И чем больше Вы--}}
            {{--будете заниматься, тем лучше будут Ваши результаты. Хотите проверить? Приходите на мои курсы и я покажу Вам--}}
            {{--как это работает!--}}

        {{--</p>--}}
    </div>
    <div class="col-md-1 ">

    </div>
    <div class="col-md-5">
        <img src="/images/results/result3.jpg" class="img-fluid mx-auto" style="padding-top: 50px"
             alt="Generic placeholder image">
        <img src="images/results/result6.jpg" class="img-fluid mx-auto" style="padding-top: 50px"
             alt="Generic placeholder image">

    </div>
</div>

<div class="row beforafter">
    <div class="col-md-5 ">

        <img src="images/results/result1.jpg" class="img-fluid mx-auto" data-src="holder.js/500x500/auto"
             style="padding-top: 230px" alt="Generic placeholder image">

    </div>
    <div class="col-md-2 ">

    </div>
    <div class="col-md-5">
        <img src="images/results/result2.jpg" class="featurette-image img-fluid mx-auto"
             data-src="holder.js/500x500/auto" style="padding-top: 230px" alt="Generic placeholder image">

    </div>
</div>

<div class="row beforafter">
    <div class="col-md-5">

        <img src="images/results/result4.jpg" class="featurette-image img-fluid mx-auto"
             data-src="holder.js/500x500/auto" style="padding-top: 230px" alt="Generic placeholder image">

    </div>
    <div class="col-md-2"></div>
    <div class="col-md-5">
        <img src="images/results/result5.jpg" class="featurette-image img-fluid mx-auto"
             data-src="holder.js/500x500/auto" style="padding-top: 230px" alt="Generic placeholder image">

    </div>
</div>