<div id="aboutme" class="row featurette">
    <div class="col-md-7 order-md-2 lead rounded-heading">
        <h2 class="featurette-heading">
          About me.
        </h2>
        @lang("msg.aboutme_header1")

    </div>
    <div class="col-md-5 order-md-1">
        <img src="/images/aboutme/vpr.jpg" class="featurette-image img-fluid mx-auto" data-src="holder.js/500x500/auto"
             style="padding-top: 180px" alt="Generic placeholder image">
        <br>
        <img src="/images/aboutme/certificate.png" class="featurette-image img-fluid mx-auto"
             data-src="holder.js/500x500/auto" style="padding-top: 50px; transform: scaleX(-1); width:65%;"
             alt="Generic placeholder image">
        <img src="/images/aboutme/cert1.jpg" class="featurette-image img-fluid mx-auto"
             data-src="holder.js/500x500/auto" style="padding-top: 50px; " alt="Generic placeholder image">
    </div>
</div>