<div class="row featurette">
    <div class="col-md-7 lead rounded-heading">
        <h2 class="featurette-heading">
            {{--Что такое красота? <span class='text-muted'>И какова её цена?</span>--}}
            @lang("msg.before_header2")
        </h2>
        @lang("msg.before_header3")
        {{--<p>--}}
            {{--Es ist allgemein bekannt, dass Schönheit ein teureres Vergnügen ist. Und natürliche Schönheit wird nicht--}}
            {{--jedem gegeben. Auf der Suche nach Schönheit ist das weibliche Geschlecht bereit, den letzten Cent zu opfern.--}}
            {{--Nicht viele Frauen glauben, dass Face-Fitness nur wenig Zeit und Fleiß erfordern, dafür aber einen guten--}}
            {{--Coach. Er wird Ihnen helfen, die für Sie richtigen Übungen zu finden, ihre Hände richtig zu platzieren und--}}
            {{--erklären, welche Gefühle Sie bei den Übungen haben sollten.<br>--}}
            {{--Wenn Sie einmal die richtige Funktionsweise Ihrer Muskeln erlernt haben und wissen, damit umzugehen, werden--}}
            {{--Sie nie mehr daran denken, einen Kosmetiker für Injektionen oder gar einen plastischen Chirurgen--}}
            {{--aufzusuchen. <br>--}}
            {{--Es gibt aber auch einige Dinge, die nicht mit Face-Fitness korrigiert werden können.--}}
            {{--ABER! Das Beibehalten einer schönen Gesichtskontur, das Öffnen der Augen, das Verhindern von Tränensäcken--}}
            {{--sowie der Schwerkraft, das Glätten der Nasolabial- und Zornesfalten, können durch das Training mit einem--}}
            {{--professionellen Coach erreicht werden. Danach ist es unter Anleitung einfach die gewünschten Ziele zu--}}
            {{--erreichen. Die Zeit und das Geld sind im Vergleich zu anderen Vorgehensweise keine große Investition. Ich--}}
            {{--wünsche Ihnen ein schönes Gesicht!--}}
        {{--</p>--}}
    </div>
    <div class="col-md-5">
        <img src="images/krasota1.jpg" class="featurette-image img-fluid mx-auto" data-src="holder.js/500x500/auto"
             style="padding-top: 230px" alt="Generic placeholder image">
    </div>
</div>