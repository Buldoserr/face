<div id="home" class="row featurette">
    <div class="col-md-7 lead rounded-heading">
        <h2 class="featurette-heading">
            @lang("msg.aboutface_header")
            {{--Что такое Face-Fitness?--}}
            {{--<span class="text-muted">It'll blow your mind.</span>--}}
        </h2>
        @lang('msg.aboutface_header1')
        {{--<p>--}}
            {{--<b>Face-Fitness </b> ist eine sehr effektive Gymnastik für das Gesicht. Es besteht aus einer einzigartigen--}}
            {{--Reihe von Übungen, die darauf abzielen, die Gesichtsmuskeln auf eine spezifische Art und Weise zu--}}
            {{--trainieren. Damit wird nicht nur die Muskulatur im Gesicht aufgebaut, sondern es wird sich darum gekümmert,--}}
            {{--diese sorgfältig und mit Hilfe von ausgewählten Übungen, im Gleichgewicht zu halten. Schwache Muskeln werden--}}
            {{--gestärkt und angespannte entspannt. </p>--}}
        {{--<p>--}}
            {{--Körper-Fitness hat einen sehr hohen Stellenwert in unserer Gesellschaft, allerdings wird das Gesicht dabei--}}
            {{--vergessen. Und dabei ist das Gesicht unsere Visitenkarte! Face-Fitness, die spezielle Gymnastik für die--}}
            {{--Gesichtsmuskulatur, hilft Falten zu glätten, macht die Haut elastischer, und die Gesichtszüge werden--}}
            {{--gestrafft und der Tonus des muskulösen Gesichtsrahmens zurückgegeben.<br>--}}
            {{--Mit Hilfe von Face-Fitness erreichen Sie:</p>--}}


        {{--<ul tyle='list-style-type: none;'>--}}
            {{--<li><span>✓</span> eine gesunde Haut, die nicht mit einer Abdeckcreme bedeckt werden muss</li>--}}
            {{--<li><span>✓</span> eine Verbesserung der Hautfarbe</li>--}}
            {{--<li><span>✓</span> eine verbesserte Kontur des Gesichts</li>--}}
            {{--<br>--}}
            {{--<li><span>✓</span> ein perfektes Aussehen vor einem wichtigen Ereignis</li>--}}
            {{--<li><span>✓</span> eine Verbesserung der Nasolabialfalten, Zornesfalten, des Doppelkinns (Lefzen und--}}
                {{--Schwellungen sind damit Geschichte)--}}
            {{--</li>--}}
            {{--<li><span>✓</span> die Falten auf der Stirn und um die Augen werden beseitigt, die Augenlider werden--}}
                {{--gestrafft--}}
            {{--</li>--}}

            {{--<li><span>✓</span> die Schwellungen und Tränensäcke unter den Augen werden entfernt</li>--}}
            {{--<li><span>✓</span> der Prozess der Augenabsackung wird verlangsamt</li>--}}
            {{--<li><span>✓</span> die Kontur und das Volumen der Lippen wird verändert</li>--}}
            {{--<li><span>✓</span> die Wangenknochen werden ausdrucksvoller</li>--}}
            {{--<li><span>✓</span> die Falten am Hals geglättet</li>--}}
            {{--<br>--}}
            {{--<li><span>✓</span> es werden Energietechniken verwendet, um die Stimmung zu verbessern und die Selbstliebe--}}
                {{--zurückzugewinnen--}}
            {{--</li>--}}
            {{--<li><span>✓</span> Immer perfekt auf Selfies aussehen, ohne Filter zu verwenden</li>--}}
        {{--</ul>--}}
        {{--<p>--}}
            {{--Die Muskeln im Gesicht benötigen Nahrung, Sauerstoff und Bewegung. Sie sind sehr sanft und geschmeidig. Es--}}
            {{--ist wichtig Muskeln pflegend und sorgfältig zu behandeln, denn auch die Creme wird auf spezielle Linien--}}
            {{--aufgetragen. Das Ziel der Face-Fitness ist die korrekte Gewebespannung auszubalancieren.<br>--}}

            {{--Face-Fitness kann in jedem Alter betrieben werden, es ist eine Philosophie, die einen das ganze Leben--}}
            {{--begleitet! Es ist die Kultur der Schönheit, Gesundheit und Jugend.--}}
        {{--</p>--}}
    </div>
    <div class="col-md-5">
        <img src="/images/upr.jpg" class="featurette-image img-fluid mx-auto" data-src="holder.js/500x500/auto"
             style="padding-top: 150px" alt="Generic placeholder image">
        <img src="/images/home/home1.jpg" class="featurette-image img-fluid mx-auto" data-src="holder.js/500x500/auto"
             style="padding-top: 50px" alt="Generic placeholder image">
        <img src="/images/home/home2.jpg" class="featurette-image img-fluid mx-auto" data-src="holder.js/500x500/auto"
             style="padding-top: 50px" alt="Generic placeholder image">
    </div>
</div>