<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="first-slide" src="/images/main/main1.jpg" alt="First slide">

            <div class="container">
                <div class="carousel-caption  text-right">
                    <h1>
                        @lang("msg.carousel_header1")

                    </h1>
                    <p>
                        @lang("msg.carousel_content1")

                    </p>
                    {{--<p><a class="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p>--}}
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <img class="second-slide" src="/images/main/main2.jpg" alt="Second slide">
            <div class="container">
                <div class="carousel-caption text-right">
                    <h1>
                        @lang("msg.carousel_header2")

                    </h1>
                    <p>

                        @lang("msg.carousel_content2")

                    </p>
                    <p><a class="btn btn-lg  schedule" href="/enterbonus" role="button">To bonuses</a></p>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <img class="third-slide" src="/images/main/main3.jpg" style="padding-left: 68%; padding-right: 3%;"
                 alt="Third slide">
            <div class="container">
                <div class="carousel-caption text-left">
                    <h1>
                        @lang("msg.carousel_header3")

                    </h1>
                    <p>
                        @lang("msg.carousel_content3")

                    </p>
                    <p><a class="btn btn-lg  schedule" href="/beforeafter" role="button">Browse before-after</a></p>
                </div>
            </div>
        </div>
    </div>
    <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>