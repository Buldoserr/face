<div class="col-sm-8 rounded-heading lead">

    <h2>
        {{--Зарегистрироваться на мероприятие --}}
        @lang('msg.register_header1')
    </h2>
    <p class="lead">
        {{$event->date}}
    </p>

    <h3>
        {{$event->event_type['name_'.getLocal()]}}
    </h3>
    <p>
        {{$event->place['name_'.getLocal()]}}
    </p>

    <form method="POST" action="/register/{{$event->id}}">
        {{csrf_field()}}

        <div class="form-group">

            <label for="name">Full Name:</label>

            <input type="text" class="form-control" id="name" name="name" required>


        </div>

        <div class="form-group">

            <label for="email">Email:</label>

            <input type="email" class="form-control" id="email" name="email" required>

        </div>

        <div class="form-group">

            <label for="phone">Telephone:</label>

            <input type="tel" class="form-control" id="phone" name="phone" required>

        </div>


        <div class="form-group">

            <button type="submit" class="btn btn-primary schedule">Register</button>

        </div>

        {{--@include('layouts.errors')--}}


    </form>


</div>