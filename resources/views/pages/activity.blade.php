<h2  id="activitytable" class="featurette-heading">
    {{--Актуальное расписание:--}}
    @lang('msg.schedule_header1')
</h2>


<table class="table table-sm">
    <thead>
    <tr>
        <th scope="col">Date</th>
        <th scope="col">Duration</th>
        <th scope="col">Class</th>
        <th scope="col">Place</th>
        <th scope="col">Join</th>
    </tr>
    </thead>
    <tbody>
    @foreach( $events as $classf)


        @include('classes.schedule')


    @endforeach

    </tbody>
</table>

<p> @lang('msg.schedule_header2') email: <a href="mailto:anastasia@face-fitness.com">anastasia@face-fitness.com</a>  tel: +49 151 157 24475;</p>
