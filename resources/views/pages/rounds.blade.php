{{--<!-- Three columns of text below the carousel -->--}}
{{--<div id="rounds" class="row">--}}
{{--<h1>Мои Классы</h1>--}}
{{--</div>--}}

<div  id="rounds" class="row ">

    <div class="col-lg-6 rounded-heading">
        <img class="rounded-circle" src="/images/classes/class1.jpg" alt="Generic placeholder image" width="250"
             height="250">

        <h2> @lang("msg.classes_header1")</h2>
        <br>
        @lang("msg.classes_content1")

        <p><a class="btn btn-secondary schedule" href="#activitytable" role="button">Schedule &raquo;</a></p>
    </div><!-- /.col-lg-4 -->


    <div class="col-lg-6 rounded-heading">
        <img class="rounded-circle" src="/images/classes/class2.jpg" alt="Generic placeholder image" width="250"
             height="250">
        <h2>@lang("msg.classes_header2")</h2>
        <br>
        @lang("msg.classes_content2")


        <p><a class="btn btn-secondary schedule" href="#activitytable" role="button">Schedule &raquo;</a></p>
    </div><!-- /.col-lg-4 -->
</div>

<br>
<div class="row">
    <div class="col-lg-6 rounded-heading">
        <img class="rounded-circle" src="/images/classes/class3.jpg" alt="Generic placeholder image" width="250"
             height="250">
        <h2>@lang("msg.classes_header3")</h2>
        @lang("msg.classes_content3")

        {{--<p>Sofortige Wirkung von Super-Lifting und Recovery. Sie gibt der Haut sofort einen guten Teint, Farbe,--}}
        {{--vertreibt Schwellungen, "setzt" die Muskeln an Ort und Stelle, kämpft effektiv gegen altersbedingte--}}
        {{--Manifestationen.--}}
        {{--Es wird empfohlen, die Massage nicht öfter als 1 bis 3 Mal pro Woche durchzuführen. Verfügbar für alle mit--}}
        {{--einem absolvierten Basis Kurs, aber unabhängig von der Erfahrung.--}}
        {{--Für alle, die:</p>--}}
        {{--<ul style='list-style-type: none '>--}}
        {{--<li><span>✓</span> sofort den größtmöglichen Effekt erzielen möchten.</li>--}}

        {{--<li><span>✓</span> ie lernen möchten, wie sie ihrer Haut und Vitalität schnell ein frisches und gesundes--}}
        {{--Aussehen nach einer schlaflosen Nacht oder Stress zurückbringen können.--}}
        {{--</li>--}}

        {{--</ul>--}}
        {{--<div class='alert alert-info'>Die Vakuum-Geräte sind in den Kosten der Vakuum Masterclass enthalten und--}}
        {{--verbleiben danach bei Ihnen.--}}
        {{--</div>--}}

        {{--<p>Preise pro Kurs:</p>--}}
        {{--<ul style='list-style-type: none '>--}}

        {{--<li><span>✓</span> 120 Euro / Person - ab einer Gruppe von 4 Personen</li>--}}
        {{--<li><span>✓</span> 200 Euro - Personal Training</li>--}}
        {{--</ul>--}}
        {{--</p>--}}
        <p><a class="btn btn-secondary schedule" href="#activitytable" role="button">Schedule &raquo;</a></p>
    </div><!-- /.col-lg- -->


    <div class="col-lg-6 rounded-heading">
        <img class="rounded-circle" src="/images/classes/class4.jpg" alt="Generic placeholder image" width="250"
             height="250">
        <h2>@lang("msg.classes_header4")</h2>
        @lang("msg.classes_content4")
        <br> <br>
        <p><a class="btn btn-secondary schedule" href="#activitytable" role="button">Schedule &raquo;</a></p>
    </div><!-- /.col-lg-4 -->
</div><!-- /.row -->