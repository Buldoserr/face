@extends('layouts.mastercab')
@section('content')

    @include('place.create')


    <table class="table table-sm">
        <thead>
        <tr>
            <th scope="col">Place name RU</th>
            <th scope="col">Place name DE</th>
            <th scope="col">Place name EN</th>
            <th scope="col">Edit</th>
        </tr>
        </thead>

        <tbody>

        @foreach( $places as $placef)

            @include('place.place')

        @endforeach

        </tbody>
    </table>

@endsection