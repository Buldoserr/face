<div class="col-md-8 blog-main">
    <H1>Создание нового места проведения</H1>

    <hr>

    <form method="POST" action="/places">
        {{csrf_field()}}


        <div class="form-group">

            <label for="name_ru">Название места на русском</label>
            <input name="name_ru" id="name_ru" cols="30" rows="10" class="form-control" required >

        </div>

<br>
        <div class="form-group">

            <label for="name_de">Название места на немецком</label>
            <input name="name_de" id="name_de" cols="30" rows="10" class="form-control" required >

        </div>
        <br>

        <div class="form-group">

            <label for="name_en">Название места на английском</label>
            <input name="name_en" id="name_en" cols="30" rows="10" class="form-control" required >

        </div>

        <br>


        <br>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
        {{--@include('layouts.errors')--}}
    </form>
</div>