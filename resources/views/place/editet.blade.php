@extends('layouts.mastercab')
@section('content')

<div class="col-md-8 blog-main">
    <H1>Редактируем место проведения!</H1>

    <hr>

    <form method="POST" action="/places/{{$places->id}}">
        {{csrf_field()}}


        <div class="form-group">

            <label for="name_ru">Название места на русском</label>
            <input name="name_ru" id="name_ru" cols="30" rows="10" class="form-control" value="{{$places->name_ru}}" required >

        </div>

<br>
        <div class="form-group">

            <label for="name_de">Название места на немецком</label>
            <input name="name_de" id="name_de" cols="30" rows="10" class="form-control" value="{{$places->name_de}}" required >

        </div>
        <br>

        <div class="form-group">

            <label for="name_en">Название места на английском</label>
            <input name="name_en" id="name_en" cols="30" rows="10" class="form-control" value="{{$places->name_en}}" required >

        </div>


        <br>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
        {{--@include('layouts.errors')--}}
    </form>
</div>
@endsection