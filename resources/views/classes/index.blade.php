{{--Для создания новых событий календаря--}}
@extends('layouts.mastercab')
@section('content')
@include('classes.create')


<table class="table table-sm">
    <thead>
    <tr>
        <th scope="col">Date</th>
        <th scope="col">Class name</th>
        <th scope="col">Place name</th>
        <th scope="col">Duration</th>
        <th scope="col">Edit</th>
        <th scope="col">Classmates</th>
        <th scope="col">Зарегистрировалось</th>

    </tr>
    </thead>
    <tbody>
    @foreach( $events as $classf)

        @include('classes.class')

    @endforeach

    </tbody>
</table>


@endsection
