{{--Интерфейс создания новых событий календаря--}}

<div class="col-md-8 blog-main">
    <H1>Создание событий в календаре!</H1>

    <hr>

    <form method="POST" action="/classes">
        {{csrf_field()}}

        <div class="form-group">

            <label for="date">Дата</label>

            <input type="datetime-local" name="date" id="date" cols="30" rows="10" class="form-control" required>

        </div>

        <br>
        <div class="form-group">
            <label for="place_id">Место проведения</label>

            <select class="form-control" name="place_id" id="place_id" required>

                @foreach($places as $placef)
                    <option value="{{$placef['id']}}">{{$placef['name_'.getLocal()]}}</option>
                @endforeach
            </select>

        </div>

        <div class="form-group">
            <label for="event_type_id">Название мастер-класса</label>

            <select class="form-control" name="event_type_id">

                @foreach($event_types as $event_type_f)
                    <option value="{{$event_type_f['id']}}">{{$event_type_f['name_'.getLocal()]}}</option>
                @endforeach
            </select>

        </div>


        <br>

        <br>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>

    </form>
</div>
