{{--Интерфейс редактирования событий календаря.--}}

@extends('layouts.mastercab')
@section('content')
<div class="col-md-8 blog-main">
    <H1>Редактируем событие календаря</H1>

    <hr>

    <form method="POST" action="/classes/{{$event->id}}">
        {{csrf_field()}}


        <div class="form-group">

            <label for="date"> Дата</label>
            <input type="datetime-local" name="date" id="date" cols="30" rows="10" class="form-control" value="{{$event->date}}" required >

        </div>

        <br>
        <div class="form-group">
            <label for="place_id">Место проведения</label>

            <select class="form-control" name="place_id" id="place_id"  required>

                @foreach($places as $placef)
                    <option value="{{$placef['id']}}" @if($placef['id']==$event->place_id) selected @endif>

                        {{$placef['name_'.getLocal()]}}

                    </option>
                @endforeach
            </select>

        </div>

        <div class="form-group">
            <label for="event_type_id">Название класса</label>
            <select  class="form-control" name="event_type_id" id="event_type_id"  required>

                @foreach($event_types as $event_type_f)
                    <option value="{{$event_type_f['id']}}" @if($event_type_f['id']==$event->event_type_id) selected @endif>

                        {{$event_type_f['name_'.getLocal()]}}

                    </option>
                @endforeach
            </select>

        </div>


        <br>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
        {{--@include('layouts.errors')--}}
    </form>
</div>
    @endsection