@extends('layouts.mastercab')

@section('content')

    <h1>Список участников</h1>
    <p>
        Мероприятие {{$event->event_type_byid()}} <br>
        Дата {{$event->date}} <br>
        Место {{$event->place_byid()}}

    </p>

    <table class="table table-sm">
        <thead>
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Phone</th>
            <th scope="col">email</th>
        </tr>
        </thead>
        <tbody>

        @foreach($classmates as $classmate)
            <tr>

                {{--<td>{{$eventf['place_'.App::getLocale()]}}</td>--}}
                <td>{{$classmate->name}}</td>
                <td>{{$classmate->phone}}</td>
                <td>{{$classmate->email}}</td>

                {{--<td><a class="schedule btn btn-sm" href="/classes/{{$classf->id}}">Edit</a> </td>--}}
                {{--<td><a class="schedule btn btn-sm" href="/register/list/{{$classf->id}}">Список участников</a> </td>--}}

            </tr>
        @endforeach
        </tbody>
    </table>
    {{--@include('pages.bonus')--}}


@endsection
