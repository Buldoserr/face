
<p> <br> </p>
<footer class="container" id="contactor">
    <p class="float-right"><a href="#">Back to top</a></p>

    <p>Email: <a href="mailto:anastasia@face-fitness.com">anastasia@face-fitness.com</a>  Tel: +49 151 157 24475;</p>
        <p>
            <a href="https://www.facebook.com/anastasia.kirchenmajer"><img src="/images/icon/foot_fb.png"></a>
            <a href="http://www.instagram.com/facefitness_munchen.de"><img src="/images/icon/foot_ig.png"> </a>
            <a href="https://vk.com/id417132718"> <img src="/images/icon/foot_vk.png"></a>
        </p>
</footer>
<div class="container">
    <nav class="navbar navbar-expand-md navbar-dark" style="background-color: #7A4883">
        <a class="navbar-brand"  href="/">&copy; 2017-2018 Anastasia Kirchenmajer. All rights reserved. | </a>

        <a class="navbar-brand"  href="/privacy"> Privacy policy | </a>
        <a class="navbar-brand"  href="/imprint"> Imprint  | </a>



        {{--<p>&copy; 2017-2018 Anastasia Kirchenmajer. <!-- &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p> --> </p>--}}
    </nav>
</div>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

<!--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="../../assets/js/vendor/popper.min.js"></script>
<script src="../../dist/js/bootstrap.min.js"></script> -->
<!-- Just to make our placeholder images work. Don't actually copy the next line! -->
<script src="../../assets/js/vendor/holder.min.js"></script>