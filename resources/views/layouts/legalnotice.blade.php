<div class="privacy">
        <h1>LEGAL NOTICE</h1> <br>

         <h4>Information according to § 5 of the Telemedia Act (Telemediengesetz – TMG):</h4>
            <br>
    <div>
            <strong> Face-fitness Anastasia Kirchenmajer </strong><br>
            <p>Forststrasse 35 <br>
                    82069 Hohenschäftlarn, Germany
            </p>

            <strong>Represented by:</strong> <br>
            <p>Anastasia Kirchenmajer</p>

            <strong> Contact details:</strong> <br>

            <p>
                Telephone: +49 151 15724475 <br>
                Telefax: +49 (89) 54 76 79 - 29 <br>

                <a href="mailto:anastasia@face-fitness.com"> anastasia@face-fitness.com</a> <br>
            </p>

            <p>
                <strong> Register entry: </strong><br>
                TAX number:  <span style="color: darkred">  </span>
            </p>
            </div> <br>




            <h4>Online dispute resolution pursuant to Art. 14 para. 1 of the Regulation on Online Dispute Resolution in Consumer Affairs (ODR-VO):</h4>
            <p>
                The European Union has set up an online dispute resolution platform ('OS platform') for out-of-court settlement of consumer disputes, which can be found under the following URL: <a href="http://www.ec.europa.eu/consumers/odr">www.ec.europa.eu/consumers/odr</a>. Our email address is written as: <a href="mailto:anastasia@face-fitness.com"> anastasia@face-fitness.com</a>
                We are neither willing nor obliged to attend a consumer arbitration board.

            </p>

            <h4>The entity responsible for the content (responsible party in the sense of the German Press Law (Pressegesetz) – V.i.S.d.P.) (pursuant to § 55 of the Interstate Broadcasting Agreement (Rundfunkstaatsvertrag – RStV)):</h4>
            <p>
                As the content provider, Face-fitness Anastasia Kirchenmajer is responsible for its own content, which it keeps ready for use pursuant to the general laws. If contributions are indicated by name, these reflect the opinions of the respective authors and therefore not the opinion of Face-fintess Anastasia Kirchenmajer.
            </p>


            <h4>Exclusion of liability/disclaimer:</h4>
            <p>
                Cross-references ('external links') should be discerned from Face-fitness Anastasia Kirchenmajer own content in terms of the content available by other content providers. When setting up external links, this involves vital and dynamic cross references. The setting up of external links does not mean that Face-fitness Anastasia Kirchenmajer embraces the content behind the external link. Face-fitness Anastasia Kirchenmajer keeps only third-party content available for use through the external link. Face-fitness Anastasia Kirchenmajer is only responsible for this third-party content if it has definite knowledge of legal violations (i.e. especially regarding illegal or criminal content), and it is technically possible and reasonable therefor to prevent its use. Upon connecting to the external links for the first time, Face-fitness Anastasia Kirchenmajer had checked the third-party content as to whether any possible civil or criminal responsibility has been caused thereby. However, it cannot be ruled out that the content could be modified subsequently by the respective providers. Face-fitness Anastasia Kirchenmajer is not obliged to constantly check for modifications of the third-party content—to which it refers on its website—which could re-establish responsibility. Constant monitoring of the external links is not reasonable for Face-fitness Anastasia Kirchenmajer without concrete evidence of legal violations. The respective provider of these pages is solely liable for such additional content and, in particular, for damages resulting from the use or non-use of such presented information, however not that which only refers to the respective publication via links. Only Face-fitness Anastasia Kirchenmajer establishes or is advised by others that a concrete offer, to which it has provided a link, causes a civil or criminal liability, shall it cancel the reference to this offer as far as this is technically possible and reasonable therefor.
            </p>

            <h4>Notice of copyright and neighbouring rights</h4>

            <p>
                The works published on this website (e.g. texts, graphics and images), as well as the design and layout of the website, are subject to German copyright law. Any kind of duplication, processing, distribution and exploitation outside the restrictions of copyright law requires the prior written consent of the author, with the exception of downloads and copies of this site for private, non-commercial permitted use. The consent requirement applies in particular to the duplication, revision, translation, storage, processing and reproduction of content in databases or other electronic media and systems. The unauthorised duplication or transmission of individual content or complete pages is prohibited and punishable. Copyright violations and the infringement of copyright exploitation rights shall be prosecuted under both civil and criminal law. Under German law, injunction and damages, reimbursement of expenses, the transfer, removal or destruction of the reproductions as well as public notice of a ruling are possible. Legal prosecution according to legal systems of other countries is possible.
                The presentation of this website in external frames is only permitted with prior written permission.
                Insofar as the contents on this page have not been created by the provider, the copyrights of third parties are respected. In particular, the content of third parties shall be marked as such. We kindly ask you to notify us should you nevertheless become aware of a copyright infringement. Upon notification of such violations, we shall remove the content in question immediately.
            </p>


            <STRONG> Photo credits: </STRONG>
            <p style="color: darkred">Fotografin <br>
                Helga Helleberg<br>
                mobile: 0151-22830103<br>
                email: <a href="mailto:helga.helleberg@gmx.de">helga.helleberg@gmx.de</a><br>
            <a href="http://www.helgahelleberg.de/">http://www.helgahelleberg.de/</a>
            </p>

            <strong>Concept/design: </strong>
            <p style="color: darkred">Andrey Pasoshnikov <br>
                Nordendstr. 43 Rgb <br>
                80801 München<br>
                Germany</p>

            <a href="/privacy">PRIVACY POLICY</a>
</div>