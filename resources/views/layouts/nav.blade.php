
<header>
    <!---<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">-->
        <nav class="navbar navbar-expand-md fixed-top navbar-dark " style="background-color: #7A4883">
        {{--<nav class="navbar navbar-expand-md navbar-dark" style="background-color: #7E2542">--}}
        {{--<a class="navbar-brand" href="#">Face-fitness</a>--}}
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" style="padding-left: 23%" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="/#home">Home  <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/about#aboutme">About me </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="/beforeafter#results">Results</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="/schedule#rounds">Classes</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="#contactor">Contacts</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="/enterbonus">Bonus</a>
                </li>
            </ul>

            <!---<form class="form-inline mt-2 mt-md-0">
                <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form> -->
        </div>

        <div class="collapse navbar-collapse right" id="navbarCollapse2">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="/lang/ru">Русский <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/lang/de">Deutsch</a>
                </li>
                 {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" href="/lang/en">  </a>--}}
                {{--</li>--}}

            </ul>

        </div>
    </nav>
</header>