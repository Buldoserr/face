<div class="privacy">

    <h1>PRIVACY POLICY </h1>

<ol>
    <li> <h3>A. Object of this Privacy Policy</h3>
    <p class="lead">
 We appreciate your interest in our online presence and the offers on our website.
 The protection of your personal data is of great importance to us. We would therefore like to inform you in detail in this Statement as to which data is collected during your visit to our website and for the use of our offers found there, and how it is processed or utilised by us subsequently. We also describe the accompanying protective actions that we have taken in terms of technical and organisational measures.
 As provided for by the GDPR, we shall inform you in accordance with Article 5 and Article 13 of the cited Regulations, pursuant to the transparency requirement, about the nature, scope and purpose of the data processing as well as of the legal basis which legitimises the data processing. Furthermore, we shall indicate the duration of the respective data processing as well as your objection or revocation options, which are linked to the specific data protection-relevant measure in each case. If no right of objection may be granted due to technical reasons (because the processing of the data is absolutely necessary for the technical operation of the website), we assure that no data on your part shall be stored by us.
 Your other rights, which you can exercise independently of the specific measure, can be found in the section entitled 'Rights of the data subject' at the end of this Privacy Policy. Transparency is also indicated therein-irrespective of the specific measure-because of your option of objection and revocation.
    </p>


        <h4> Responsible office/service provider</h4>
        <p class="lead">
    The responsible office within the meaning of the General Data Protection Regulation (GDPR), the (new) Federal Data Protection Act (Bundesdatenschutzgesetz(-neu) - BDSG-(neu)), the State Statutory Data Protection Regulations of Niedersachsen (landesrechtlichen Datenschutzbestimmungen von Niedersachsen - NDSG) and service providers within the meaning of the Telemedia Act (Telemediengesetz - TMG) is:
        </p>
        <div> Anastasia Kirchenmajer <br>
             Forststrasse 35, 82069 Hohenschäftlarn, Deutschland<br>
             Phone: +49 (0)151 15724475<br>
             <a href="mailto:anastasia@face-fitness.com"> anastasia@face-fitness.com</a> <br>
             Website: <a href="http://www.face-fitness.com" >www.face-fitness.com</a>
             <a href="/imprint">(See our Legal Notice)</a> <br>
        </div> <br>
 <p class="lead"> If you have any questions or comments about this Privacy Policy or about data protection in general, please write to the following email address: <a href="mailto:anastasia@face-fitness.com"> anastasia@face-fitness.com</a>
</p>

        <h4> Legal basis for the processing of personal data </h4>
 <p class="lead"> Insofar as we seek the consent of data subjects for the processing operations of personal data, Art. 6 para. 1 lit. a of the GDPR serves as the legal basis for processing of the personal data.
 As regards the processing of personal data which is necessary for the performance of a contract to which the data subject is a contractual party, Art. 6 para. 1 lit. b of the GDPR serves as the legal basis. This also applies to processing operations which are necessary for the performance of pre-contractual measures.
 Insofar as the processing of personal data is required to fulfil a legal obligation to which our company is subject, Art. 6 para. 1 lit. c of the GDPR serves as the legal basis.
 In the event that the vital interests of the data subject or another natural person require the processing of personal data, Art. 6 para. 1 lit. d of the GDPR serves as the legal basis.
 If processing is required to safeguard a legitimate interest of our company or a third party, and the interests, fundamental rights and freedoms of the data subject do not outweigh the initial interest, Art. 6 para. 1 lit. f of the GDPR serves as the legal basis for the processing.
 </p>

        <h4>Data erasure and storage duration</h4>
        <p class="lead">
 The personal data of the data subject shall be erased or blocked as soon as the purpose of the storage is no longer applicable. Moreover, the data shall be stored if such storage has been designated by the European or national legislator in Union regulations, laws or other provisions to which the responsible body is subject. Unless there is a need for further storage of the data for contract conclusion or contract fulfilment, the data shall be blocked or erased once the storage period prescribed by the indicated standards expires.
        </p>

</li>
<li> <h3> B. Processing operations relevant to data protection</h3>

<p class="lead">

     The collection and use of your data through visiting our website
     Description of the nature and scope of the data processing
     In the case of merely using the website for information purposes (i.e. you do not register with us or otherwise provide us with information), we only collect the data which your browser automatically transmits to our server. The submission of this information is required in order to display the website.
     The following data is thus collected:
</p>
    <ul class="lead">

                    <li>       Information about the browser type and the version used </li>

                    <li>       Operating system of the user </li>

                    <li>       Internet service provider of the user </li>

                    <li>       IP address of the user </li>

                    <li>       Date and time of access </li>

                    <li>       Websites via which the system of the user reaches our website </li>

                    <li>       Websites that are accessed by the user's system via our website </li>
    </ul>
 <p class="lead">
  In addition to the automated transmission of data, this is also stored in the log files of our system. This data is not stored together with other personal data of the user. </p>
 <h4>The purpose of data processing</h4>
<p class="lead">
 Storage in log files is carried out to ensure the functionality of the website. Moreover, the data serves to ensure the security of our information technology systems. There is no evaluation of the data for marketing purposes within this context.
 This also presents our legitimate interest in data processing for this purpose in accordance with Art. 6 para. 1 lit. f of the GDPR.
 </p>
 <h4>Legal basis</h4>
<p class="lead">
 The temporary storage of the automatically-created and technically-dependent necessary log files shall be lawfully carried out according to Art. 6 para. 1 lit. f of the GDPR. The legitimate interest is accounted for in the purpose of the processing.
</p>
 <h4>Duration of the data processing</h4>
 <p class="lead">
   In the case of data storage in log files, this shall be the case after seven days at the latest. Storage for an extended period is possible. In this case, the IP addresses of the users are erased or manipulated such that an association with the visiting client is no longer possible.
 </p>

 <h4>Objection and revocation option</h4>
 <p class="lead">The collection of the data for website deployment and the storage in log files is compulsory for the operation of the website. There is no option of objection in this respect.</p>

 <h3>Email communication</h3>

 <h4>Description of the nature and scope of the data processing</h4>
 <p class="lead">
  In the case of email communication via the email address displayed on our website, we collect, process and store personal data such as the surname, first name, title where applicable, postal address, email and IP address of the user, as well as the date and time of registration, in order to provide the respective services, to get in contact, to process your enquiry and in the event that follow-up questions arise.
 When making contact via the provided email address, the personal data of the user which is submitted along with the email shall be stored.
  There is no disclosure of data to third parties in this context. The data is used exclusively for processing of the conversation.
 </p>

 <h4>The purpose of data processing</h4>
 <p class="lead">When contact is made via email, the required legitimate interest in data processing is present in this case as well.</p>

 <h4>Legal basis for the data processing</h4>
 <p class="lead">Art. 6 para. 1 lit. f of the GDPR is the legal basis for processing the data which is sent as part of an email. If the email contact is aimed towards the conclusion of a contract, the additional legal basis for the processing is Art. 6 para. 1 lit. b of the GDPR. </p>

 <h4>Duration of the storage</h4>
 <p class="lead">The data shall be erased as soon as it is no longer necessary for achieving the purpose of its collection. In terms of personal data that has been sent via email, such is the case if the respective conversation with the user has ended. The conversation is ended when it can be inferred from the circumstances that the relevant issues have finally been clarified. The personal data additionally collected during the sending process shall be erased after a period of seven days at the latest.
 Objection and revocation option
 If the user gets in contact with us by email, he/she can object to the storage of his/her personal data at any time. In such a case, the conversation can no longer continue. All personal data that has been stored over the course of making contact shall be erased in such a case.
 </p>
 <h4>Security notice</h4>
 <p class="lead">
  With regard to making contacting by email, we would like to note that data transfer over the Internet presents issues with security holes in email communication and cannot be completely protected against access by third parties. Personal data in the framework of email communication shall usually be transmitted from your computer via an unsecured connection over the Internet. Information that you send unencrypted by email could be read, stored and misappropriated by third parties along the way. We would therefore like to point out that no confidential information should be sent to us without the use of an encryption program.
 </p>

 <h3>Social plug-ins</h3>

 <h4>Nature and scope of the data collection</h4>
 <p class="lead">
  We currently use the following social media plug-ins: Facebook.
  We also use the so-called two-click solution. This means that when you visit our site, generally no personal data is initially passed on to the providers of the plug-ins. The provider of the plug-in can be identified by the marking on the box above its initial letter or logo. We give you the opportunity to communicate directly with the provider of the plug-in via the button. Only once you click on the marked field thereby activating it, shall the plug-in provider receive the information that you have accessed the corresponding website of our online offering. In addition, the following data is transmitted:
 </p>
<ul class="lead">
         <li> IP address </li>
         <li>The date and time of access</li>
         <li>Time zone difference to Greenwich Mean Time (GMT)</li>
         <li>Content of the request (concrete page)</li>
         <li>Access status/HTTP status code</li>
         <li>The transmitted amount of data in each case</li>
         <li>Website from which the request comes</li>
         <li>Browser</li>
         <li>Operating system and its interface</li>
         <li>Language and version of the browser software</li>

</ul>
 <p class="lead" >In the case of Facebook, according to the statement of the respective provider in Germany, the IP address shall be anonymised immediately after collection. By activating the plug-in, personal information about you shall be transmitted to the respective plug-in provider and stored there (in the USA in the case of US providers). Because the plug-in provider carries out data collection via cookies in particular, we recommend that you delete all cookies before clicking on the greyed out boxes via the security settings of your browser. </p>
 <p class="lead"> We have no influence on the collected data and data processing operations, nor are we aware of the full extent of the data collection, the purpose of the processing or the retention periods. Moreover, we have no information on the erasure of the data collected by the plug-in provider.</p>

 <h4>Purpose of the data collection </h4>
 <p class="lead"> The plug-in provider stores the data collected about you as usage profiles and applies this for purposes of advertising, market research and/or the needs-based design of its website. Such an evaluation is carried out in particular (including for users who are not logged in) for the presentation of needs-based advertising and in order to inform other social network users about your activities on our website. In terms of the plug-ins, we offer you the opportunity to interact with social networks and other users, so that we can improve our offer and make it more attractive for you as a user.</p>

 <p class="lead"> The data transfer takes place regardless of whether or not you have an account with the plug-in provider and are logged in there. If you are logged in with the plug-in provider, your data collected by us shall be assigned directly to your existing account with the plug-in provider. If you press the activated button and, for instance, link the page, the plug-in provider shall also store this information in your user account and share it publicly with your contacts. We recommend that you log out regularly after using a social network, especially so before activating the button in order to avoid any association of your profile by the plug-in provider. </p>

<p class="lead"> Further information on the purpose and scope of the data collection and its processing by plug-in providers can be found in the data protection statements of these providers as listed below. There you can also find more information about your related rights and configuration options for the protection of your privacy. </p>

<h4> Addresses of the respective plug-in providers and URL with their data protection notices: </h4>
 <p class="lead"> Facebook Inc., 1601 S California Ave, Palo Alto, California 94304, USA;<br>
 <a href="http://www.facebook.com/policy.php">http://www.facebook.com/policy.php </a>;<br>
 further information on data collection:<br> <a href="http://www.facebook.com/help/186325668085084"> http://www.facebook.com/help/186325668085084</a>,<br>
 <a href="http://www.facebook.com/about/privacy/your-info-on-other#applications"> http://www.facebook.com/about/privacy/your-info-on-other#applications </a> as well as <br>
 <a href="http://www.facebook.com/about/privacy/your-info#everyoneinfo"> http://www.facebook.com/about/privacy/your-info#everyoneinfo.</a> <br>
  Facebook has submitted to the EU-US Privacy Shield, <br>
  <a href="https://www.privacyshield.gov/EU-US-Framework"> https://www.privacyshield.gov/EU-US-Framework.</a></p>
<p class="lead">
 Instagram LLC, represented by Kevin Systrom and Mike Krieger, 1601 Willow Rd, Menlo Park CA 94025, USA, <br>
 <a href="https://help.instagram.com/155833707900388/?helpref=hc_fnav&bc[0]=Instagram-Hilfe&bc[1]=Datenschutz%20und%20Sicherheitsbereich"> https://help.instagram.com/155833707900388/?helpref=hc_fnav&bc[0]=Instagram-Hilfe&bc[1]=Datenschutz%20und%20Sicherheitsbereich</a> <br>
 <a href="https://help.instagram.com/155833707900388/?helpref=hc_fnav&bc%5b0%5d=Instagram-Hilfe&bc%5b1%5d=Datenschutz%20und%20Sicherheitsbereich">https://help.instagram.com/155833707900388/?helpref=hc_fnav&bc%5b0%5d=Instagram-Hilfe&bc%5b1%5d=Datenschutz%20und%20Sicherheitsbereich</a>

</p>

 <h4>Legal basis for the data collection</h4>
 <p class="lead">The legal basis for the use of plug-ins is Art. 6 para. 1 sentence 1 lit. f of the GDPR. </p>

 <h4>Objection and revocation option</h4>
 <p class="lead">
  You have the right to object to the formation of these user profiles, whereby you must contact the respective plug-in provider to exercise this right.<br>
  We recommend that you log out regularly after using a social network, especially so before activating the button in order to avoid any association of your profile by the plug-in provider.
 </p>

 <h4>Transfer of data to third parties</h4>
 <p class="lead"> Your personal data shall be passed on, where applicable, to supporting service providers which have been carefully selected by us. These may be technical service providers, service providers supporting us in distribution or third-party companies involved in data processing (so-called outsourced data processing, external data processors). If we use external data processors, they are contractually obligated-under observance of the data protection regulations and according to our instructions-to handle your personal data with care, and neither to use it for their own purposes nor to pass it on to third parties.<br>
 Otherwise, your data shall be transferred to other third parties by us only if we are legally obliged to do so. Due to legal requirements, we are obliged to pass on your personal data to third parties in certain cases. Such is the case, for instance, if there is suspicion of a crime or misuse of this website. We are then required to disclose your personal data to the competent law enforcement authorities. Upon an order by the competent authorities, we may therefore provide information on this data in individual cases to the extent that this is necessary for the purposes of law enforcement, danger prevention, the fulfilment of statutory duties of constitutional protection authorities or the military counter-intelligence, or for the enforcement of intellectual property rights.
 </p>
 <h4>Duration of the storage</h4>
 <p class="lead">Your personal data shall only be stored for as long as it is necessary for the required purpose. The personal data shall be erased as soon as it is no longer necessary to fulfil the purpose of the storage.</p>
</li>
 <li> <h3>C. Rights of the data subject</h3>
 <p class="lead">
    Should any personal data be processed about you, you are considered to be the data subject in the sense of the GDPR and you are entitled to the following rights with respect to the responsible body:
 </p>

  <h4>The right to information</h4>
  <p class="lead">
    You may request confirmation from the responsible body as to whether any personal data relating to you has been processed by us. <br>

  Should any such processing have been carried out, you may request information on the following details from the responsible body:
  </p>
            <ul class="lead">
             <li>the purposes for which the personal data is processed;</li>
             <li>the personal data categories that are being processed;</li>
             <li>the recipients or categories of recipients to whom the personal data relating to you have been disclosed or are still being disclosed;</li>
             <li>the planned duration of storage of the personal data relating to you or, if specific information is not available in this respect, the criteria for determining the duration of storage;</li>
             <li>the existence of a right to the rectification or erasure of the personal data relating to you, a right to the restriction of processing by the responsible body or a right to object to such processing;</li>
             <li>the existence of a right of complaint to a supervisory authority;</li>
             <li>all available information on the origin of the data, if the personal data is not collected from the data subject;</li>
             <li>the existence of automated decision-making including profiling under Art. 22 para. 1 and 4 of the GDPR and-at least in these cases-meaningful information about the logic involved as well as the consequences and the intended impact of such processing on the data subject.</li>
            </ul>

 <p class="lead">
 You have the right to request information about whether any personal data relating to you is being transferred to a third country or to an international organisation. In this context, you may request to be notified about the applicable guarantees under Art. 46 of the GDPR in connection with the transfer.
 </p>
  <br>
  <h4>The right to rectification</h4>
 <p class="lead">
  You have a right to rectification and/or completion with respect to the responsible body, provided that the processed personal data relating to you is inaccurate or incomplete. The responsible body must implement the rectification without delay.<br>
 </p>
  <h4>The right to restriction of processing </h4><br>
 <p class="lead">
   You may request to restrict the processing of the personal data relating to you under the following conditions:
 </p>
    <ul class="lead">
        <li>if you dispute the accuracy of the personal data relating to you for a duration which would allow the responsible body to verify the accuracy of the personal data;</li>
        <li>the processing is unlawful and you decline the erasure of the personal data, and instead request the restriction of the use of the personal data;</li>
        <li>the responsible body no longer needs the personal data for processing purposes, however you need it to assert, exercise or defend legal claims; or</li>
        <li>if you have objected to the processing pursuant to Art. 21 para. 1 of the GDPR, and it has not yet been determined as to whether the legitimate reasons of the responsible body outweigh your own reasons.</li>

    </ul>

 <p class="lead"> If the processing of the personal data relating to you has been restricted, such data may-apart from its storage- be processed solely with your consent or for the assertion, exercise or defence of legal claims, or for the protection of the rights of another natural or legal entity, or for reasons of significant public interest of the Union or of a Member State. <br>
  If the restriction of the processing was implemented in accordance with the above-mentioned conditions, you shall be notified by the responsible body before the restriction is lifted. </p>

  <h4>The right to erasure</h4>
  <strong>Obligation to erase</strong>
  <p class="lead">You may request from the responsible body that the personal data relating to you be erased immediately. The responsible body is then obliged to erase this data immediately, provided that any of the following applies: </p>
     <ul class="lead">
          <li>The personal data relating to you is no longer necessary for the purposes for which it was collected or otherwise processed.</li>
          <li>You revoke your consent to the processing in accordance with Art. 6 para. 1 lit. a or Art. 9 para. 2 lit. a of the GDPR, and there is no other legal basis for the processing.</li>
          <li>You object to the processing pursuant to Art. 21 para. 1 of the GDPR and there are no overriding legitimate reasons for the processing, or you object to the processing pursuant to Art. 21 para. 2 of the GDPR.</li>
          <li>The personal data relating to you has been processed unlawfully.</li>
          <li>The erasure of the personal data relating to you is required to fulfil a legal obligation under Union law or the law of the Member States to which the responsible body is subject.</li>
          <li>The personal data relating to you was collected based on the offered services of the information society pursuant to Art.8 para. 1 of the GDPR.</li>
     </ul>
<br>
  <h4>Information to third parties </h4>
 <p class="lead">
 If the responsible body has publicly disclosed the personal data relating to you and has an obligation to the erasure of such pursuant to Art. 17 para. 1 of the GDPR, it shall-taking into account the available technology and the implementation costs-take the appropriate measures, including those of a technical nature, to inform the responsible body who processes the personal data that you, as a data subject, have requested of them the deletion of all links to this personal data, or copies or replications of this personal data.
 </p>

  <h4>Exceptions </h4>

  <p class="lead">There is no right to erasure if the processing is necessary </p>
       <ul class="lead">
          <li>to exercise the right to freedom of expression and information;</li>
          <li>to fulfil a legal obligation which requires the processing under the law of the Union or of the Member States to which the responsible body is subject, or arises for the performance of a task which is in the public interest or in the exercise of official authority, which has been assigned to the responsible body;</li>
          <li>for reasons of public interest in the field of public health pursuant to Art. 9 para. 2 lit. h and i as well as Art. 9 para. 3 of the GDPR;</li>
          <li>for archival purposes in the public interest, scientific or historical research purposes, or for statistical purposes according to Art. 89 para. 1 of the GDPR, provided that the right mentioned under section a) prospectively renders impossible or seriously impairs the realisation of the objectives of this processing; or</li>
          <li>for the assertion, exercise or defence of legal claims.</li>
       </ul>
<br>
  <H4>The right to information</H4>
 <p class="lead">If you have asserted the right to rectification, erasure or restriction of processing with respect to the responsible body, the latter is obliged to communicate this rectification or erasure of data or the restriction of processing to all recipients to whom the personal data relating to you has been disclosed, unless this proves to be impossible or involves disproportionate effort. <br>
  You are entitled to the right, with respect to the responsible body, to be notified about these recipients. </p>
  <h4>The right to data portability</h4>
  <p class="lead">You have the right to obtain the personal data relating to you, which you have provided to the responsible body, in a structured, commonplace and machine-readable format. Moreover, you have the right to transfer this data to another responsible body without interference by the responsible body to whom the personal data had been provided, as long as</p>
    <ul class="lead">
      <li>the processing is based on consent pursuant to Art. 6 para. 1 lit. a of the GDPR or Art. 9 para. 2 lit. a of the GDPR, or on a contract pursuant to Art. 6 para. 1 lit. b of the GDPR, and</li>
      <li>the processing is executed using automated procedures.</li>
    </ul>
 <br>
     <p class="lead">
 In exercising this right, you also have the right to issue that the personal data relating to you is transferred directly from one responsible body to another responsible body, provided that this is technically feasible. The freedoms and rights of other persons may not be affected thereby.<br>
 The right to data portability does not apply to the processing of personal data which is required for the performance of a task that is in the public interest or arises in the exercise of official authority, which has been transferred to the responsible body.
     </p>

  <h4>The right of objection</h4>
<p class="lead"> You have the right to object at any time-for reasons that arise from your particular situation-to the processing of personal data relating to you, which is carried out on the basis of Art. 6 para. 1 lit. e or f of the GDPR. This also applies to profiling based on these provisions. Please direct your objection to our data protection officers. <br>
 The responsible body shall no longer process the personal data relating to you, unless it can prove compelling legitimate grounds for the processing which outweigh your interests, rights and freedoms, or the processing serves the assertion, exercise or defence of legal claims. <br>
 If the personal data relating to you is processed in order to engage in direct advertising, you have the right to object to the processing of the personal data relating to you at any time for the purposes of such advertising; this also applies to profiling insofar as it is associated with such direct advertising. <br>
 Should you object to the processing for direct marketing purposes, your personal data shall no longer be processed for these purposes. <br>
 You have the option, in connection with the use of services of the information society and notwithstanding Directive 2002/58/EC, to exercise your right to object by means of automated procedures, whereby technical specifications are used. <br>
</p>

  <h4>Right to revoke the data protection consent declaration </h4>
  <p class="lead">You have the right to revoke your data protection consent declaration at any time. By revoking the consent, the legality of the processing carried out on the basis of consent shall not be affected up until the revocation. </p>

  <h4>Automated decision on an individual basis including profiling </h4>
 <p class="lead"> You have the right not to be subjected to a decision based solely on automated processing, including profiling, which produces legal effects that impact you or which significantly affects you in a similar manner. This does not apply if the decision</p>
    <ul class="lead">
       <li>is required for the conclusion or performance of a contract between you and the responsible body;</li>
       <li>is permissible on the basis of Union or Member State legislation to which the responsible body is subject, and this legislation contains appropriate measures for safeguarding your rights and freedoms as well as your legitimate interests; or</li>
       <li>is made with your express consent.</li>
    </ul>
  <br>
 <p class="lead">However, these decisions may not be based on special personal data categories pursuant to Art. 9 para. 1 of the GDPR, unless Art. 9 para. 2 lit. a or g applies and reasonable measures have been taken to protect the rights and freedoms as well as your legitimate interests.
 As regards the first and third exceptions, the responsible body shall take appropriate measures to uphold the rights and freedoms as well as your legitimate interests, which involves at least the right to obtain the intervention of a person on the part of the responsible body, upon the statement of its own position and upon the challenge to the decision.
 </p>

  <h4>The right to complain to a supervisory authority </h4>
 <p class="lead">Without prejudice to any other regulatory or judicial remedy, you have the right to complain to a supervisory authority, in particular in the Member State of your place of residence, place of work or place of alleged infringement, if you believe that the processing of the personal data relating to you violates the GDPR. <br>
 The supervisory authority, to which the complaint was lodged, shall inform the complainant on the status and results of the complaint, including the possibility of a judicial remedy pursuant to Art. 78 of the GDPR.
 </p>
 </li>
 <li> <h3>D. Data protection for hyperlinks to external websites</h3>
  <p class="lead">
 We take no responsibility for the confidential handling of your data should you find hyperlinks on our website that take you directly onto the website of another provider (e.g. recognisable by the change of URL). Such is the case since we have no control over the compliance with data protection regulations by these companies. Please refer to the websites directly to find out about the handling of your personal data by these companies.
  </p>

 <li><h3>E. Data security</h3>
 <p class="lead">
  We use technical and organisational security measures to protect incoming or collected personal data about you, in particular against accidental or intentional manipulation, loss, destruction and against attacks by unauthorised persons. Our security measures are continuously improving in line with technological developments.
 </p>
 </li>
</ol>
</div>