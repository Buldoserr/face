<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Face Fitness in Munich</title>

    <!-- Bootstrap core CSS -->
    <!--<link href="../../dist/css/bootstrap.min.css" rel="stylesheet"> -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="/css/carusel.css" rel="stylesheet">

</head>
<body>

@include('layouts.navcab')

<main role="main">




    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing">

        <!-- Three columns of text below the carousel -->
        {{--@include('pages.rounds')--}}

        <!-- START THE FEATURETTES -->

        {{--<hr class="featurette-divider">--}}

        @yield('content')
                {{--@include('pages.aboutface')--}}


        {{--<hr class="featurette-divider">--}}


                {{--@include('pages.aboutme')--}}


        {{--<hr class="featurette-divider">--}}


                {{--@include('pages.beauty')--}}


        {{--<hr class="featurette-divider">--}}

        <!-- /END THE FEATURETTES -->

    </div><!-- /.container -->


    <!-- FOOTER -->

</main>
@include('layouts.footer')
</body>
</html>
