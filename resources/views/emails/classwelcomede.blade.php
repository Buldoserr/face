<!DOCTYPE html>

<html>

<head>

    <title></title>
</head>

<body>
{{--<h1>    Welcome to class! </h1>--}}
{{--<p> You are registered to event {{$event_type}} as {{$classmate->name}}</p>--}}

{{--<p>The master class starts on {{$date}} in {{$place}}</p>--}}
{{--<p> I am going to call you by phone or through the messangers to accept your registration and provide the details.</p>--}}

{{--<p> If it is not you, who was registered to my event on www.face-fitness.com just reply me with your rejecting information.</p>--}}


<div>
    <h1> Herzlich Willkommen zum Workshop! </h1>
    <p> Sie sind für den folgenden Kurs angemeldet: {{$event_type}}.</p>

    <p>Ort und Zeit des Workshops: </p>

    <p>{{$place}}</p>

    <p>{{$date}} </p>

    <p> Ich werde mich telefonisch bei Ihnen melden, um die Registrierung zu bestätigen und weitere Details zu
        besprechen.</p>

    <p> Sollten sie nicht der korrekte Empfänger dieser Email sein, bitte informieren Sie uns und löschen diese
        Email. </p>
</div>

<p>Anastasia</p>
<p>
    www.face-fitness.com
</p>
</body>

</html>